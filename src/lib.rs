#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

use std::fs::{ File, OpenOptions };
use std::io::{ prelude::*, Write };

/// Contains the rules to to control a device (fan)
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ControlProfile {
    pub id: usize,
    pub description: String,
    pub steps: Vec<Step>
}

/// The sensor/fan environment, serialized as json formatted config file. May be renamed to Config
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Environment {
    pub description: String,  
    pub sleep: u64,
    pub pipe_infos: Vec<PipeInfo>,
    pub fans: Vec<Fan>,
    pub control_profiles: Vec<ControlProfile>,
    pub sensors: Vec<Sensor>
}

impl Environment {
    pub fn get_pipe_info(&self, id: usize) -> Option<PipeInfo> {
        self.clone().pipe_infos.into_iter().find(|a| a.id == id)
    }
    pub fn get_profile(&self, id: usize) -> Option<ControlProfile> {
        self.clone().control_profiles.into_iter().find(|a| a.id == id)
    }
    pub fn get_sensor(&self, id: usize) -> Option<Sensor> {
        self.clone().sensors.into_iter().find(|a| a.id == id)
    }
    pub fn get_fan(&self, id: usize) -> Option<Fan> {
        self.clone().fans.into_iter().find(|a| a.id == id)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Fan {
    pub id: usize,
    pub description: String,
    pub path: String,
    pub input_suffix: String,
    pub label_suffix: String,
    pub manual_suffix: String,
    pub output_suffix: String,
    pub prefix: String
}
impl Fan {
    pub fn read_rpm(&self) -> u16 {
        let path = format!( "{}{}{}{}",
                self.path.to_string(),
                self.prefix,
                self.id.to_string(),
                self.output_suffix.to_string()
            );
            let mut current_rpm = String::new(); 
            let mut f = File::open(path).expect(stringify!(std::io::ErrorKind::NotFound));
            f.read_to_string(&mut current_rpm).expect("Error: read to string.");
            current_rpm.trim_end().parse::<u16>().unwrap() 
    }
    pub fn set_rpm(&self, rpm: u16) -> Result<String, std::io::Error> {
        if self.below_max(rpm) {
            let path = format!( "{}{}{}{}",
                self.path.to_string(),
                self.prefix,
                self.id.to_string(),
                self.output_suffix.to_string()
            );
            let mut f = OpenOptions::new().append(false).write(true).open(path).expect(stringify!(std::io::ErrorKind::NotFound) );        
            
            match f.write_all(rpm.to_string().as_bytes()) {
                Err(why) => Err(why),
                Ok(_) => Ok( format!("{:?}{:?} rpm set to: {:?} ", self.prefix, self.id, rpm) ),
            }
        } else {
            Err( std::io::Error::new(std::io::ErrorKind::InvalidData, "Error: Target rpm above fan max speed.") )
        }
    }
    pub fn below_max(&self, rpm: u16) -> bool {
        let path = format!( "{}{}{}{}",
            self.path.to_string(),
            self.prefix,
            self.id.to_string(),
            "_max".to_string()
        );
        let mut max_rpm = String::new(); 
        let mut f = File::open(path).expect(stringify!(std::io::ErrorKind::NotFound));
        f.read_to_string(&mut max_rpm).expect("Error: read to string.");
     
     //   if rpm < max_rpm.trim_end().parse::<u16>().unwrap() { true } else { false }
        rpm < max_rpm.trim_end().parse::<u16>().unwrap()
    }
}

/// Wraps the fan control flow in a single struct. With more sophisticated environment the Pipe helps to
/// manage different setups and access to its properties.
/// 
/// #Examples
/// ```
/// let pipe = Pipe{ sensor: s.clone(), control_profile: cp.clone(), fan: f.clone()} ;
/// ```
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PipeInfo {
    pub id: usize,
    pub description: String,
    pub sensor_id: usize,
    pub fan_id: usize,
    pub profile_id: usize 
}

/// A k/v pair of the ControlProfile property steps
/// 
/// # Examples
/// 
/// If the sensed temperaure is above 37°C, the rpm should be 2040rpm.
/// ```
/// { 
///     "temp_limit": 37,
///     "rpm": 2040
/// }
/// ```
/// 
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Step {
    pub temp_limit: u8,
    pub rpm: u16
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Sensor {
    pub id: usize,
    pub active: bool,
    pub description: String,
    pub input_suffix: String,
    pub label_suffix: String,
    pub path: String,
    pub prefix: String
}

impl Sensor {
    pub fn sense(&self, mut temperature: Temperature) -> Option<Temperature> {
        if self.active {
            let path = [ self.path.to_string(), self.prefix.to_string(), self.id.to_string(),
            self.input_suffix.to_string() ].concat();
            let mut content = String::new(); 
            let mut f = File::open(path).expect(stringify!(std::io::ErrorKind::NotFound));
            f.read_to_string(&mut content).expect("Error: read to string.");
            temperature.previous = temperature.current;
            temperature.current = content.trim_end().parse::<i32>().unwrap();
            Some(temperature) 
        } else {
            None
        }
    }
}

/// The previously and currently measured temperature value of a ```Sensor```. It can be used for instance to 
/// reduce the read and writes, if temperature value of the previous cycle is compared to the acutal value.
/// 
/// # Example
/// 
/// ```
/// if temp.previous != temp.current { 
///     // no action needed (ie calc_rpm or adjust) if temperature hasn't changed.
/// }
/// ```
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Temperature {
    pub current: i32,
    pub previous: i32
}

impl Temperature {
    pub fn new(current: i32, previous: i32) -> Self {
        Temperature { current, previous }

    }
}
impl Default for Temperature {
    fn default() -> Self {
        Temperature::new(0, 0)
    }
}

