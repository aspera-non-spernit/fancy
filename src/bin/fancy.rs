extern crate fancy;
extern crate clap;
extern crate syslog;

use fancy::{Environment, Temperature};
use std::{
    fs::{ File, OpenOptions },
    io::{ prelude::*, Write },
    sync::mpsc,
    time::Duration,
    thread
};

use clap::{App, load_yaml};

// use syslog::{Facility, Severity};

/// Loads the config file. ```init``` is currently used to set prerequisites. For instance to set the fan control to manual mode
/// 
/// Should be invoked before all other functions. Alternatively the environment can be setup programatically.
/// 
/// #Examples
/// ```
/// let env: &Environment = &fancy::init(String::from("/home/{USER_NAME}/Development/fancy/fancy.conf"));
/// ```
pub fn init(cfg_file: &str) -> Result<Environment, String> {
    println!("Initializing fancy..");
    let mut contents = String::new(); 
    let mut f = File::open(cfg_file).expect(stringify!(std::io::ErrorKind::NotFound) );
    f.read_to_string(&mut contents).expect("Error reading config file");
    let env = match serde_json::from_str::<Environment>(&contents) {
        Ok(env) => env,
        Err(e) => panic!(e.to_string())
    };
    // set fans to manual control once
    for pipe in &env.pipe_infos {
        match &env.get_fan(pipe.fan_id) {
            Some(fan) => {
                let path = format!( "{}{}{}{}",
                fan.path.to_string(),
                fan.prefix.to_string(),
                fan.id.to_string(),
                fan.manual_suffix.to_string() );
                let mut f = OpenOptions::new().append(false).write(true).open(path).expect(stringify!(std::io::ErrorKind::NotFound) );  
                // writing 1 to /sys/devices/platform/apple../fan{id}_manual file
                match f.write_all("1".to_string().as_bytes()) {
                    Err(why) => println!("{:?}", why ),
                    Ok(_) => println!("{:?}{:?} set to manual control", fan.prefix, fan.id ),
                }      
            },
            None => println!("Fan {:?} not found. Probably caused by an error in the Pipe section of environment file.", pipe.fan_id),
        }
    }
    Ok(env)
}

fn run(env: &Environment) {
    let dur = Duration::from_millis(env.sleep);
   
    let (tx, rx) = mpsc::channel();

    let cc = (0..env.pipe_infos.len()).map(|worker_id| {
        let envc = env.clone();
        let txc = tx.clone();
        let handle = thread::spawn(move || {
            let mut temperature = Temperature::default();
            let pipe = &envc.pipe_infos[worker_id];     //not to be confused with pipe id            
            let mut current_rpm = envc.get_fan(pipe.fan_id).unwrap().read_rpm();
            loop {
                temperature = envc.get_sensor(pipe.sensor_id).unwrap().sense(temperature.clone()).unwrap();
                let mut msg = format!("Pipe {} {:?} RPM: {}", pipe.id, &temperature, &current_rpm);
                let txl = txc.clone();
                txl.send(msg).unwrap();
                if temperature.current != temperature.previous { 
                    let steps = envc.get_profile(pipe.profile_id).unwrap().steps;
                    let mut rpm = current_rpm;
                    for (i, step) in steps.iter().enumerate() {
                        if i32::from(step.temp_limit) > temperature.current / 1000 {
                            rpm = steps[i - 1].rpm;
                            break;
                        }
                    }
                    if current_rpm != rpm {
                        envc.get_fan(pipe.fan_id).unwrap().set_rpm(rpm);
                        msg = format!("Pipe {}: Setting RPM from {} to {}", pipe.id, current_rpm, rpm);
                        txl.send(msg).unwrap();
                        current_rpm = rpm;                        
                        
                    }
                }
                thread::sleep(dur);
            }
           
        }); // end spawn

        handle
    }).collect::<Vec<thread::JoinHandle<()>>>();
    loop {
        println!("{}", rx.recv().unwrap());

    }
    for handle in cc {
        handle.join().unwrap();
        println!("Joining workers from the main thread");

    }
/*
    for pipe_info in env.pipe_infos.clone() {
        let envc = env.clone();
        let tx = mpsc::Sender::clone(&tx);
        handles.push(
            std::thread::spawn(move || {
                let mut temperature = Temperature::default();
                let mut current_rpm = envc.get_fan(pipe_info.fan_id).unwrap().read_rpm();
                loop {
                    temperature = envc.get_sensor(pipe_info.sensor_id).unwrap().sense(temperature.clone()).unwrap();
                    if temperature.current != temperature.previous {
                        let steps = envc.get_profile(pipe_info.profile_id).unwrap().steps;
                        let mut rpm = current_rpm;
                        for (i, step) in steps.iter().enumerate() {
                            if i32::from(step.temp_limit) > temperature.current / 1000 {
                                rpm = steps[i - 1].rpm;
                                break;
                            }
                        }
                        let mut msg = format!("Pipe {} {:?}", pipe_info.id, temperature);
                        tx.send(msg).unwrap();
                        // write new rpm only if changed
                        if current_rpm != rpm {
                            current_rpm = rpm;
                            envc.get_fan(pipe_info.fan_id).unwrap().set_rpm(rpm);
                            // msg = format!("Pipe {}: RPM: {} {}", pipe_info.id, current_rpm, temperature.current);
                            // tx.send(msg).unwrap();
                        }
                    }
                }
                thread::sleep(dur);     
            })
        );            
    }
    println!("{}", rx.recv().unwrap());
    for handle in handles {
        // Wait for the thread to finish. Returns a result.
        let _ = handle.join().expect("Thread stopped unexpectedly.");
    }
    */
}

fn main() -> Result<(), String> {
    let yaml = load_yaml!("../../config/cli.yml");
    let matches = App::from(yaml).get_matches();
    
    let env = init( matches.value_of("profile").unwrap() )?; // profile required option TODO: sep manual from init
  
    if matches.is_present("temp") {
        println!("Note: Running fancy with --temp flag does not conrtol the fan!");
        loop {
            let mut active = vec![];
            let mut inactive = vec![];
            for sensor in &env.sensors {
                if sensor.active { active.push(sensor); }
                else { inactive.push(sensor); }
            }
            println!("-- ACTIVE SENSORS --");
            active.iter()
                .for_each( |s| println!("{:?} {:?}", s.description, s.sense(Temperature::default()).unwrap().current ));
            println!("-- SENSORS NOT ACTIVE --");
            inactive.iter()
                .for_each( |s| println!("{:?} {:?} ", s.id, s.description) );
            thread::sleep(Duration::from_millis(env.sleep));
        }
    } else {
        run(&env);
    }
    Ok(())
}