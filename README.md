![](https://img.shields.io/badge/Made%20with-rust%20v1.46.0%20(04488afe3%202020----08----24)-red)

# fancy - Fan Control YEAH

Fancy is a fan control program written in Rust. Tested on Arch Linux installed on a iMac Early 2008.

Since I installed Arch Linux on my old iMac, I realized that one of the fans in the machine was always running on full power (3300 RPM).
The fan control described in the Arch Wiki did not work (no pwmconfig error). Also the macfanctl wasn't satisfactory.
Therefore I began to write a new program / lib. 

# DISCLAIMER

Use with caution. The provided profile config works on my machine, an early 2008 iMac running Arch Linux. 
Running ```fancy``` on a different machine may harm the fans or the components the fans are supposed to cool.

If you are adding your own profile configuration, check the /sys/devices/platform/../{fanN_.., inputN..} files or install the program
```sensors``` to find out how many sensors and fans are installed in your machine.

The ```/sys/devices/``` folder is explicitly created as interface to such devices. Files that are supposed to be protected by the system cannot be overridden.

See: https://www.kernel.org/doc/Documentation/filesystems/sysfs.txt

# Usage 

Currently fancy is not available in package managers You need to compile fancy from source using ```rust```.

## Compile

### Install rust

Visit https://www.rust-lang.org/tools/install and follow the steps. It's simple.

**Note** The following may be outdated.

```bash
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Download and build ***fancy***

```bash
$ git clone https://gitlab.com/aspera-non-spernit/fancy.git
$ cd fancy
$ cargo build --release

// optional strip the binary. It makes the file smaller. Note that fancy may not display error messages.

$ strip target/release/fancy
```

## Run as root

You can run ```fancy``` from any folder, but ```fancy``` overrides files in the ```/sys/devices/..``` folder.
Therefore you need write permissions for that folder, as root does.

You may copy the compiled  binary to the ```/usr/local/bin/fancy```, 

**Examples**

Show help

```bash
# fancy -- help
fancy 0.2.0

Profile based fan control

USAGE:
    fancy [FLAGS] --profile <FILE>

FLAGS:
    -h, --help       Prints help information
    -t, --temp       Displays current temperatures
    -V, --version    Prints version information

OPTIONS:
    -p, --profile <FILE>    Sets a custom config file
```

Display current temperatures from the sensors without controlling the fans

```bash
# fancy --temperature --profile etc/fancy/profiles/imac-early-2008.conf
Initializing fancy..
Note: Running fancy with --temp flag does not conrtol the fan!
-- ACTIVE SENSORS --
"TA0P - Ambient Pipe" 22500
"TC0H - CPU 0 Heatpipe" 39250
"TG0D - GPU 0 Die" 51750
"TC0H - GPU 0 Heatpipe" 48500
"TG0H - GPU 0 Heatpipe" 48000
"TG0P - HDD 0 Proximity" 44750
"TL0P - LCD 0 Proximity" 49000
"TO0P - ODD 0 Proximity" 36250
"TW0P - Wireless 0 Proximity" 52500
"Tm0P - Battery Charger 0 Proximity" 43750
"Tp0P - Power Supply 0 Proximity" 71250
-- SENSORS NOT ACTIVE --
2 "TC0D - CPU 0 Die" 
4 "TC0P - CPU 0 Proximity" 
8 "TG1D - GPU 1 Die"
```

## Install as systemd Service
There's no installation routine available. You have to copy a few files as root.

All files required to run ```fancy``` as a systemd service are provided.

If you have not installed rust previously. Install the defaults and execute the source command.

**Note** The following steps must be executed as privileged user (root or sudo)

### Copy files

Copy the profile configuration file the compiled binary and the service file.

```bash
# mkdir -p /usr/local/etc/fancy/profiles
# cp etc/fancy/profiles/* /usr/local/etc/fancy/profiles

# cp target/release/fancy /usr/local/bin

# cp usr/lib/systemd/system/fancy.service /etc/systemd/system
```

Enable and start the the fancy service

```bash
# systemctl enable fancy
# systemctl start fancy

```

Check if fancy runs

```bash
# journalctl -b -u fancy
```
or

```bash
# systemctl start fancy
```

## Uninstall

If you have used ***fancy*** as sevice

```bash
# systemctl stop fancy
# systemctl disable fancy
```

Then just delete all related files and directories

```bash
# rm -rf /usr/local/etc/fancy
# rm /usr/local/bin/fancy
# rm /etc/systemd/system/fancy.service
```

## The config file

fancy makes use of a JSON formatted config file. It contains properties 

- for each sensor available in the machine
- for each fan available in the machine
- user specific control profiles

The config file has been created by hand. The sensor and fan properties (and in particular the MIN and MAX rpm) were taken from: 

```bash
$ sensors
```

I created a control profile for each fan, because ```sensors``` returned that each fan has a different set of MIN / MAX rpm speeds. 

**No guarantee** that the config is working on a different machine and how I use the computer (browsing the web, youtube etc.)
For some, it might be too adaptable (fan speed changes too often), or doesn't change
at all, if the CPU/GPU is working heavily.

## Contribution

Like all my projects ```fancy``` is WIP and contribution is highly appreciated.
I am a Rust beginner. Code cleaning or improvements will surely be pulled.

